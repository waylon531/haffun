# Haffun

### The Haskell Funge Interpreter

Haffun is a funge interpreter written in Haskell. Currently 
the only funge supported is Befunge, but Haffun is written to be
extensible to n-dimensional funges.

## Usage

`haffun` uses the cabal build system.

`cabal v2-build` builds `haffun`

`cabal v2-run haffun -- <program>` runs the specified program

`haffun <program>` runs a program if you've already built a copy of haffun

## Further documentation

Some more documentation is provided in `doc/`. Currently this consists of a 
document describing how to implement a new funge, this
document also includes some 
description of the design and a few examples.
