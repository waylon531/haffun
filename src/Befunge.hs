{-# LANGUAGE MultiParamTypeClasses #-}

module Befunge where 

import Board
import Stack
import Cursor

newtype Grid = Grid [[ Data ]]
data Point = Point Int Int

instance Index Grid Point where
    read  (Grid l) (Point x y) 
        -- If we're in bounds return the data
        | x <  (length (l !! y))  = (l !! y) !! x
        -- Otherwise return a space
        | otherwise               = ' '
                                
    -- Insert our new element in the middle of the list
    -- by dissecting it
    write (Grid l) (Point x y) d = Grid $ (lhs) ++ ((innerl ++ (d:innerr)):rhs)
            where (lhs,h:rhs) = splitAt y l
                  (innerl,j:innerr) = splitAt x h

    wrap (Grid l) (Point x y) = Point (x `mod` longest) (y `mod` (length l))
        where longest = (maximum . (map length)) l

newtype Direction = Direction (Point->Point)
instance Directible Direction Point where
    next (Direction d)   = d
    -- We either move up or down be the specified
    -- amount each tick
    go (x1:y1:_) = Direction (\(Point x y) -> Point (x+x1) (y+y1))


newBefunge :: [String] -> Board Point Grid Direction
newBefunge s = Board (Grid s) (Cursor (Point 0 0) (Direction (\(Point x y)->(Point (x+1) y)))) newStack False
