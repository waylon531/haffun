{-# LANGUAGE MultiParamTypeClasses #-}

module Board where

import Stack
import Cursor
import Data.Char (chr,ord)
import Data.Maybe
import Control.Monad.Loops (iterateUntilM)


-- B is indexed by A
class  Index b a  where
  -- Returns data from the board
  read            :: b -> a -> Data
  -- Returns a new board
  write           :: b -> a -> Data -> b
  -- Wraps a back around if it goes over
  -- the boundaries of the board, or
  -- just returns it if we don't have to wrap
  wrap            :: b -> a -> a

newBoard :: ((Index b a), (Directible d a)) => b -> (Cursor a d) -> Board a b d
newBoard space cursor = Board space cursor newStack False


-- b is a grid that must be indexable by an a
-- d is a direction that takes us to new a's
data ((Index b a), (Directible d a)) => 
     Board a b d = Board b (Cursor a d) Stack Bool

-- Runs our funge program by continuously stepping
run :: ((Index b a), (Directible d a)) => Board a b d -> IO( Maybe (Board a b d) )
run board = iterateUntilM isNothing fromIO (Just board)

fromIO :: ((Index b a), (Directible d a)) => 
           Maybe (Board a b d) -> IO( Maybe (Board a b d))
-- Stop stepping if we get nothing back
fromIO Nothing = return Nothing
-- Otherwise run the program for one step
fromIO (Just board)= step board

--isJustIO :: IO ( Maybe a ) -> Bool
--isJustIO x = do a <- x
--                isJust a

-- Step runs our board for a single step
step :: ((Index b a), (Directible d a)) => Board a b d -> IO ( Maybe(Board a b d) )
step (Board b (Cursor p dir) s s_mode) = 
        -- First get the current character
    do  let c = Board.read b p
        -- Then figure out what instruction to run and run it
        result <- doInstruction (Board b (Cursor p dir) s s_mode) c
        case result of
            Nothing -> return Nothing
            Just (Board b (Cursor p dir) s s_mode) -> do
                -- Go to the next instruction
                -- We can potentially handle jumps here with an if statement
                let nextCursor  = Cursor (wrap b (next dir p)) dir
                return ( Just (Board b nextCursor s s_mode) )



doInstruction :: ((Index b a), (Directible d a)) => 
                 Board a b d -> Data -> IO ( Maybe (Board a b d) )
                 

-- Turn string mode on or off
doInstruction (Board b cur s True) '"' = return ( Just (Board b cur s False))

doInstruction (Board b cur s False) '"' = return ( Just (Board b cur s True))

doInstruction (Board b cur s True) c = return ( Just (Board b cur (push s (ord c)) True))

-- Hard control flow
doInstruction (Board b (Cursor p dir) s s_mode) '^' = 
    return ( Just (Board b (Cursor p (go ([0,-1]++zList))) s s_mode))

doInstruction (Board b (Cursor p dir) s s_mode) 'v' = 
    return ( Just (Board b (Cursor p (go ([0,1]++zList))) s s_mode))

doInstruction (Board b (Cursor p dir) s s_mode) '<' = 
    return ( Just (Board b (Cursor p (go ([-1,0]++zList))) s s_mode))

doInstruction (Board b (Cursor p dir) s s_mode) '>' =
    return ( Just (Board b (Cursor p (go ([1,0]++zList))) s s_mode))

-- Branches
doInstruction (Board b (Cursor p dir) (h:s) s_mode) '|' =
    if h == 0 then return ( Just (Board b (Cursor p (go ([0,1]++zList))) s s_mode))
              else return ( Just (Board b (Cursor p (go ([0,-1]++zList))) s s_mode))
            
doInstruction (Board b (Cursor p dir) (h:s) s_mode) '_' =
    if h == 0 then return ( Just (Board b (Cursor p (go ([1,0]++zList))) s s_mode))
              else return ( Just (Board b (Cursor p (go ([-1,0]++zList))) s s_mode))

-- Not
doInstruction (Board b c (h:s) s_mode) '!' =
    if h == 0 then return ( Just (Board b c (1:s) s_mode))
              else return ( Just (Board b c (0:s) s_mode))

-- GreaterThan
doInstruction (Board b c (h1:h2:s) s_mode) '`' =
    if h2>h1  then return ( Just (Board b c (1:s) s_mode))
              else return ( Just (Board b c (0:s) s_mode))

-- Add
doInstruction (Board b c (h1:h2:s) s_mode) '+' =
    return ( Just (Board b c ((h1+h2):s) s_mode))
            
-- Mul
doInstruction (Board b c (h1:h2:s) s_mode) '*' =
    return ( Just (Board b c ((h1*h2):s) s_mode))
            
-- Divide
doInstruction (Board b c (h1:h2:s) s_mode) '/' =
    return ( Just (Board b c ((h2 `div` h1):s) s_mode))
            
-- Modulus
doInstruction (Board b c (h1:h2:s) s_mode) '%' =
    return ( Just (Board b c ((h2 `mod` h1):s) s_mode))
            
-- Swap elements
doInstruction (Board b c (h1:h2:s) s_mode) '\\' =
    return ( Just (Board b c (h2:h1:s) s_mode))

-- Duplicate an element
doInstruction (Board b c (h1:s) s_mode) ':' =
    return ( Just (Board b c (h1:h1:s) s_mode))

-- Discard an element
doInstruction (Board b c (h1:s) s_mode) '$' =
    return ( Just (Board b c s s_mode))

-- Print a number
doInstruction (Board b c (h1:s) s_mode) '.' =
 do putStr (show h1)
    putStr (" ")
    return ( Just (Board b c s s_mode))

-- Print a character
doInstruction (Board b c (h1:s) s_mode) ',' =
 do putChar (chr h1)
    return ( Just (Board b c s s_mode))

-- Get a number
doInstruction (Board b c (h1:s) s_mode) '&' =
 do intString <- getLine
    let int = ( Prelude.read intString :: Int )
    return ( Just (Board b c (int:s) s_mode))

-- Get a character
doInstruction (Board b c (h1:s) s_mode) '~' =
 do char <- getChar
    let char_num = ord char
    return ( Just (Board b c (char_num:s) s_mode))

-- Halt the program
doInstruction (Board b c (h1:s) s_mode) '@' =
    return Nothing

-- Push a number on the stack
-- PRAISE VIM
doInstruction (Board b c (s) s_mode) '0' =
    return ( Just (Board b c (0:s) s_mode))
doInstruction (Board b c (s) s_mode) '1' =
    return ( Just (Board b c (1:s) s_mode))
doInstruction (Board b c (s) s_mode) '2' =
    return ( Just (Board b c (2:s) s_mode))
doInstruction (Board b c (s) s_mode) '3' =
    return ( Just (Board b c (3:s) s_mode))
doInstruction (Board b c (s) s_mode) '4' =
    return ( Just (Board b c (4:s) s_mode))
doInstruction (Board b c (s) s_mode) '5' =
    return ( Just (Board b c (5:s) s_mode))
doInstruction (Board b c (s) s_mode) '6' =
    return ( Just (Board b c (6:s) s_mode))
doInstruction (Board b c (s) s_mode) '7' =
    return ( Just (Board b c (7:s) s_mode))
doInstruction (Board b c (s) s_mode) '8' =
    return ( Just (Board b c (8:s) s_mode))
doInstruction (Board b c (s) s_mode) '9' =
    return ( Just (Board b c (9:s) s_mode))

-- NOP
doInstruction b _ = return ( Just b )

-- Our grid elements can either be Instructions or numbers
-- data Data = Instruction Instruction | Integer Integer
type Data = Char

-- This makes it a bit easier to stick an infinite amount of
-- zeros at the end of a direction
zList :: [Int]
zList = repeat 0
