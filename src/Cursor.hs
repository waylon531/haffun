{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}

module Cursor where 
-- A cursor has a location and a Direction
data (Directible d a) => Cursor a d = Cursor a d

-- How does setting a direction work?
-- d is a direction, a is a point
class Directible d a | d -> a where
    -- Takes a point and goes to the next point
    next      :: d -> a -> a
    -- Change direction on given axis
    go        :: [ Int ] -> d

