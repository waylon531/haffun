module Main where

import Board
import Unefunge
import Befunge

import System.Environment


main :: IO ()
main = do
    args <- getArgs
    let filename = args !! 0
    contents <- readFile filename
    let board = newBefunge $ lines contents
    putStrLn "Running with grid: "
    mapM_ putStrLn (lines contents)
    putStrLn ""
    --let board = newUnefunge "545**.@"
    --result <- run board

    --let board = newBefunge [ "545**.@" ]
    --result <- run board

    --let board = newBefunge [ "v@   .<",
    --                         ">784**^"]
    --result <- run board
    --return result
    result <- run board
    return ()
