module Stack where

type Stack = [ Int ]

push :: Stack -> Int -> Stack
push s d = d:s

pop :: Stack -> (Int,Stack)
pop (h:s) = (h,s) 

-- Get a stack of infinite zeroes
newStack :: Stack
newStack = repeat 0
