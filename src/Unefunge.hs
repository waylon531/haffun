{-# LANGUAGE MultiParamTypeClasses #-}

module Unefunge where 

import Board
import Stack
import Cursor

newtype Line = Line [ Data ]

instance Index Line Int where
    read  (Line l) p   = l !! p
    -- Insert our new element in the middle of the list
    -- by dissecting it
    write (Line l) p d = Line $ (lhs) ++ (d:rhs)
            where (lhs,h:rhs) = splitAt p l

    wrap (Line l) p = p `mod` (length l)

newtype Direction = Direction (Int->Int)
instance Directible Direction Int where
    next (Direction d)   = d
    -- We either move up or down be the specified
    -- amount each tick
    go (h:_) = Direction (\x -> x + h)


newUnefunge :: String -> Board Int Line Direction
newUnefunge s = newBoard 
            -- Set up a funge space from a String, a list of characters
            (Line s) 
            -- Create a cursor that starts at 0, moving to the right
            (Cursor 
                0 
                (Direction (\x->x+1))) 
